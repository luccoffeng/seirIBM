# Generate simulation output ---------------------------------------------------
#' Generate simulation output
#'
#' Generate summary statistics of the current state of the simulation.
#'
#' @param x Data.table object with four logical columns (TRUE/FALSE) for
#'   susceptible (first column), exposed, infectious and recovered status (last
#'   column).
#' @param ... Arguments passed down via \code{...} in the \code{seir}
#'   function.
#'
#' @return Returns a \code{data.table} with the following summary statistics:
#' \describe{
#'   \item{\code{S, E, I, R}}{Absolute number of susceptible, exposed, infected,
#'     and recovered individuals.}
#' }
#'
#' @import data.table
#'
#' @export
gen_output <- function(x, ...) {

  x[, lapply(.SD, sum),
    .SDcols = c("susceptible", "exposed", "infectious", "recovered")]

}


### END OF CODE ### ----
