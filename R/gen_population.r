# Generate population of agents ----
#' Generate population of agents
#'
#' Generate a population of agents. Argument values can be passed down via
#' \code{...} in the \code{seir} function.
#'
#' @param n_agent Number of agents to be generated.
#' @param alive_time Distribution for the time until an death of the individual
#'   (not related to disease). Define as a list object with named elements
#'   "name" (e.g. "gamma", "unif", "weibull") and whichever parameter names
#'   (e.g. "shape" and "rate") and values are required by the corresponding
#'   native R functions (e.g. "rgamma", "runif", "rweibull").
#' @param exposed_time Distribution for the time until an infected individual
#'   becomes symptomatic. Definition as 'alive_time'.
#' @param infected_time Distribution for the duration of symptoms. Definition as
#'   'alive_time'.
#' @param ... Arguments passed down via \code{...} in the \code{seir}
#'   function.
#'
#' @return Returns a \code{data.table} of agents.
#'
#' @import data.table
#'
#' @export
gen_population <- function(n_agent,
                           alive_time,
                           exposed_time,
                           infected_time,
                           ...) {

  if (n_agent == 0) return(NULL)

  # Initialise agent population
  pop <- data.table(age = rep(0, times = n_agent))
  pop[, age_of_death := do.call(what = paste0("r", alive_time$name),
                                args = c(list(n = .N),
                                         alive_time[-1]))]

  # Disease state (SEIR model)
  pop[, susceptible := TRUE]
  pop[, exposed := FALSE]
  pop[, infectious := FALSE]
  pop[, recovered := FALSE]
  pop[, time_till_symptoms := as.numeric(NA)]
  pop[, time_till_recovery := as.numeric(NA)]

  # Assign random durations of disease states (for if and when they occur)
  pop[, E_duration := do.call(what = paste0("r", exposed_time$name),
                              args = c(list(n = .N),
                                            exposed_time[-1]))]
  pop[, I_duration := do.call(what = paste0("r", infected_time$name),
                              args = c(list(n = .N),
                                       infected_time[-1]))]

  return(pop)
}


# Warm up the demography of population of agents ----
#' Warm up population demography
#'
#' Warm up the demogrpahy of a population of agents. Argument values can be
#' passed down via \code{...} in the \code{seir} function.
#'
#' @param x Population object generated by the \code{gen_population} function.
#' @param warmuptime Duration of warming up period of the simulation, expressed
#'   in the same time unit as the time unit of the rates in the model. This
#'   period precedes the actual simulation of transmission (argument
#'   \code{runtime}) and is intended to warm up population demography.
#' @param warmuptime_steps Integer number of discrete time steps per time unit in
#'   the warm-up phase of the simulation.
#' @param alive_time Value or distribution for the time until an death of the
#'   individual (not related to disease). Define as a list object with named
#'   elements "name" (e.g. "gamma", "unif", "weibull") and whichever
#'   parameter names (e.g. "shape" and "rate") and values are required by the
#'   corresponding native R functions (e.g. "rgamma", "runif", "rweibull").
#' @param ... Arguments passed down via \code{...} in the \code{seir}
#'   function.
#'
#' @return Does not return an object but manipulates an existing
#'   \code{data.table} of agents generated by the \code{gen_population}
#'   function.
#'
#' @import data.table
#'
#' @export
warmup_demography <- function(x,
                              warmuptime = 200,
                              warmuptime_steps = 12,
                              alive_time,
                              ...) {

  dt_warmup <- 1 / warmuptime_steps
  warmuptime <- warmuptime * warmuptime_steps

  for (i in 1:warmuptime) {
    x[, age := age + dt_warmup]
    x[age >= age_of_death,
      c("age", "age_of_death") := {
        list("age" = rep(0, times = .N),
             "age_of_death" =  do.call(what = paste0("r", alive_time$name),
                                       args = c(list(n = .N),
                                                alive_time[-1])))
      }]
  }

}

### END OF CODE ### ----

