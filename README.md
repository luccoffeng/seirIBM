# seirIBM

<!-- badges: start -->
<!-- badges: end -->

The goal of seirIBM is to provide a bare-bones individual-based implementation 
of an susceptible-exposed-infectious-recovered (SEIR) compartmental model in R.
This package is intended for didactic purposes and is not necessarily
the most computationally efficient implementation of an individual-based model
in R.

## Installation
You can install the latest released version of seirIBM from GitLab:

``` r
remotes::install_gitlab("luccoffeng/seirIBM")
```

The development version can be downloaded by:

``` r
remotes::install_gitlab("luccoffeng/seirIBM@dev")
```


## Example
``` r
rm(list = ls())
library(seirIBM)
library(data.table)



```
